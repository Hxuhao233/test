https://katex.org/docs/supported.html

test $`a'`$ test $`\varDelta`$

$`\forall`$

$`\overbrace{a+b+c}^{\text{note}}`$

```math
\sqrt[3]{x}
```


```math

```

这种公式会嵌入文本 $`a^2+b^2=c^2`$.

这种公式会渲染成单独的一行
```math
a^2+b^2=c^2
```

```math
\def\arraystretch{1.5}
\begin{array}{c:c:c}
a & b & c \\ \hline
d & e & f \\
\hdashline
g & h & i
\end{array}
```
